/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package activitystream;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author Xtra Sonic
 */
public class ActivityStream {

    FileInputStream activityFileStream;
    List<MonitoredData> activityList;
    String fileName;

    public ActivityStream(String fileName) {
        this.fileName = fileName;
        activityList = new LinkedList();
    }

    public static void main(String[] args) {
        try {
            ActivityStream run = new ActivityStream("Activities.txt");
            run.start();
        } catch (IOException | ParseException ex) {
            Logger.getLogger(ActivityStream.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private LocalDateTime convertDate(String s) throws ParseException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime date = LocalDateTime.parse(s, formatter);
        return date;
    }

    private MonitoredData readLine() throws IOException, ParseException {
        StringBuilder sb = new StringBuilder();
        char character = (char) activityFileStream.read();
        while ((int) character != 10 && (int) character != 13 && activityFileStream.available() != 0) {
            sb.append(character);
            character = (char) activityFileStream.read();
        }
        //eliminate char 13 or char 10 (whichever came second in the new line
        activityFileStream.read();

        LocalDateTime start = convertDate(sb.substring(0, sb.indexOf("		")));
        sb.delete(0, sb.indexOf("		") + 2);
        LocalDateTime finish = convertDate(sb.substring(0, sb.indexOf("		")));
        sb.delete(0, sb.indexOf("		") + 2);

        return new MonitoredData(start, finish, sb.toString().trim());

    }

    private void readFileStream() throws IOException, ParseException {
        MonitoredData activity;
        activityFileStream = new FileInputStream(fileName);
        while (activityFileStream.available() != 0) {
            activity = readLine();
            activityList.add(activity);
        }
        activityFileStream.close();
    }

    public void start() throws IOException, ParseException {
        readFileStream();

        List<LocalDate> daysList = getDistinctDays(activityList);
        try (PrintWriter pw = new PrintWriter("Counting.txt")) {
            pw.printf("%d distinct days", daysList.size());
        }

        Map<String, Integer> mapActionsCount = mapToActionCount(activityList);
        try (PrintWriter pw = new PrintWriter("MapToActionCount.txt")) {
            mapActionsCount.forEach((k, v) -> {
                pw.printf("%20s", k);
                pw.print(" -> ");
                pw.println(v);
            });
        }

        Map<Integer, Map<String, Integer>> mapPerDay = mapToDay(activityList);
        try (PrintWriter pw = new PrintWriter("MapToDay.txt")) {
            mapPerDay.forEach((k, v) -> {
                pw.print(k);
                pw.print(" -> ");
                pw.println(v);
            });
        }

        Map<String, Duration> mapActionDuration = mapToActionTime(activityList, Duration.ZERO.plus(10, ChronoUnit.HOURS));
        try (PrintWriter pw = new PrintWriter("MapToActionDuration.txt")) {
            mapActionDuration.forEach((String k, Duration v) -> {
                pw.printf("%20s", k);
                pw.print(" -> ");
                //pw.println(v);
                pw.printf("Hours: %d Minutes: %d Seconds: %d\r\n", v.toHours(), v.toMinutes() - v.toHours() * 60, v.getSeconds() - v.toMinutes() * 60);
            });
        }

        List<String> actionsMostlyUnder5Min = getActionsUnderDurationXPercent(activityList, Duration.ZERO.plus(5, ChronoUnit.MINUTES), 0.9);
        try (PrintWriter pw = new PrintWriter("ActionsUnder5min90.txt")) {
            pw.println(actionsMostlyUnder5Min);
        }
    }

    private List<String> getActionsUnderDurationXPercent(List<MonitoredData> streamList, Duration time, double ratio) {
        Map<String, List<Integer>> map;
        map = streamList.stream().collect(Collectors.toMap(
                MonitoredData::getAction,
                (MonitoredData md) -> {
                    LinkedList<Integer> valueList = new LinkedList();
                    valueList.add(1);
                    if (Duration.between(md.getLocalDateTimeStart(), md.getLocalDateTimeFinish())
                            .compareTo(time) < 0) {
                        valueList.add(1);
                    } else {
                        valueList.add(0);
                    }
                    return valueList;
                },
                (List<Integer> a, List<Integer> b) -> {
                    a.set(0,a.get(0)+b.get(0));
                    a.set(1,a.get(1)+b.get(1));
                    return a;
                }
        ));
        try (PrintWriter pw = new PrintWriter("Test.txt")) {
            map.forEach((k, v) -> {
                pw.print(k);
                pw.print(" -> ");
                pw.println(v);
            });
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ActivityStream.class.getName()).log(Level.SEVERE, null, ex);
        }
        return map.entrySet().stream()
                .filter((Entry<String, List<Integer>> e) -> {
                    return e.getValue().get(1) / e.getValue().get(0) > ratio;
                }).map((Entry<String, List<Integer>> e) ->e.getKey())
                .distinct()
                .collect(Collectors.toList());
    }

    private Map<String, Duration> mapToActionTime(List<MonitoredData> streamList, Duration limit) {
        Map<String, Duration> map;
        map = streamList.stream().collect(Collectors.toMap(
                MonitoredData::getAction,
                (MonitoredData md) -> {
                    return Duration.between(md.getLocalDateTimeStart(), md.getLocalDateTimeFinish());
                },
                (Duration d1, Duration d2) -> d1.plus(d2)
        ));
        map = map.entrySet().stream()
                .filter((entry) -> {
                    return entry.getValue().compareTo(limit) > 0;
                }).collect(Collectors.toMap(
                k -> k.getKey(),
                v -> v.getValue()
        ));
        return map;
    }

    private Map<Integer, Map<String, Integer>> mapToDay(List<MonitoredData> streamList) {

        Map<Integer, Map<String, Integer>> map;
        map = streamList.stream().collect(Collectors.toMap(
                (MonitoredData md) -> {
                    return (int) md.getLocalDateStart().hashCode();
                },
                (MonitoredData md) -> {
                    List<MonitoredData> actionsInDay;
                    actionsInDay = streamList.stream().filter((MonitoredData md2) -> {
                        return md2.getLocalDateStart().equals(md.getLocalDateStart());
                    }).collect(Collectors.toList());
                    return mapToActionCount(actionsInDay);

                },
                (s, a) -> a
        ));

        return map;
    }

    private int countAction(String s, List<MonitoredData> streamList) {
        return (int) streamList.stream().filter((MonitoredData md) -> {
            return md.getAction().equals(s);
        })
                .count();
    }

    private Map<String, Integer> mapToActionCount(List<MonitoredData> streamList) {
        Map<String, Integer> map;
        map = streamList.stream().collect(Collectors.toMap(
                MonitoredData::getAction,
                (MonitoredData md) -> 1,
                (s, a) -> s+a
        ));
        return map;
    }

    private List<LocalDate> getDistinctDays(List<MonitoredData> streamList) {
        return streamList.stream()
                .map(MonitoredData::getLocalDateStart)
                .distinct()
                .collect(Collectors.toList());

    }

}
