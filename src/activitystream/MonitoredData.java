/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package activitystream;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;




/**
 *
 * @author Xtra Sonic
 */
public class MonitoredData {
    private final LocalDateTime start;
    private final LocalDateTime finish;
    private final String action;
    
    public MonitoredData(LocalDateTime start, LocalDateTime finish, String action)
    {
        this.start=start;
        this.finish=finish;
        this.action=action;
    }

    @Override
    public String toString() {
        return "Activity{" + "start=" + start + ", finish=" + finish + ", activity=" + action + '}';
    }

    public LocalDateTime getLocalDateTimeStart() {
        return start;
    }

    public LocalDateTime getLocalDateTimeFinish() {
        return finish;
    }
    public LocalDate getLocalDateStart() {
        return start.toLocalDate();
    }

    public LocalDate getLocalDateFinish() {
        return finish.toLocalDate();
    }
    
    public String getAction() {
        return action;
    }
}
